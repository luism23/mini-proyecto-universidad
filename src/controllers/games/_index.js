require('module-alias/register')

module.exports = {
    get: require("@controllers/games/get"),
    delete: require("@controllers/games/delete"),
    post: require("@controllers/games/post"),
    put: require("@controllers/games/put")
}