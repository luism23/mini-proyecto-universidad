require('module-alias/register')
const db = require("@app/db")
const bcrypt = require('bcrypt')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const addReport = require("@functions/addReport")


const register = async (req,res)=>{

    try {
        const {
            email,
            password
        }=req.body
        
        const passwordEncrypt = bcrypt.hashSync(password, parseInt(env.SALT))
        const data = {
            email,password:passwordEncrypt
        }
        
        

        data.create_time=(new Date()).getTime()
        data.update_time=(new Date()).getTime()
    
        const colunms = Object.keys(data)
        const values = Object.values(data)
    
        const sql =`
        insert into users (${colunms.join(",")}) values('${values.join("','")}');
        `
    
        const r =await db(sql)
        delete data.password

        await addReport("","register",data)
    
        res.send({
            type : "ok",
            result : r
        })
    } catch (error) {
        return res.status(500).send({
            type : "error",
            error ,
            msj :`${error}`  
        })
    }
    
}

module.exports = register