require('module-alias/register')
const db = require("@app/db")
const bcrypt = require('bcrypt')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const jwt = require('jsonwebtoken')
const addReport = require("@functions/addReport")


const login = async (req,res)=>{
    try {
        const {
            email,
            password
        }=req.body

        const sql=`
        select * from users where email='${email}'
        `
        const r = await db(sql)

        if(r.rows.length==0){
            throw new Error("error email invalido")
        }

        const user = r.rows[0]
        const passwordEncrypt = user.password
        const respond = await bcrypt.compare(password, passwordEncrypt)

        if(!respond){
            throw new Error("error password invalido")
        }

        delete user.password

        const token = jwt.sign(user, env.JWT, { expiresIn: '2h' })

        await addReport(user.id,"login",{user,token})

        res.send({
            type : "ok",
            token
        })

    } catch (error) {
        return res.status(500).send({
            type : "error",
            error ,
            msj :`${error}`  
        })
    }
    
}

module.exports = login