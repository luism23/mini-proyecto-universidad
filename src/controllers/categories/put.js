require("module-alias/register");
const db = require("@app/db");
const addReport = require("@functions/addReport");

const putCategories = async (req, res) => {
    try {
        const { where, data } = req.body;

        data.update_time = new Date().getTime();

        const colunms = Object.keys(data);
        const values = Object.values(data);
        const sql = `
        update categories set (${colunms.join(",")}) = ('${values.join("','")}')
        where id ='${where.id}';
        `;

        const r = await db(sql);

        await addReport(req.body.users_id,"update categories",{where,data});

        res.send({
            type: "ok",
            result: r,
        });
    } catch (error) {
        return res.status(500).send({
            type: "error",
            error,
            msj: `${error}`,
        });
    }
};

module.exports = putCategories;
