require('module-alias/register')
const db = require("@app/db")
const addReport = require("@functions/addReport")



const postCategories = async (req,res)=>{

    try {
        const data = req.body

        data.create_time=(new Date()).getTime()
        data.update_time=(new Date()).getTime()

        const colunms = Object.keys(data)
        const values = Object.values(data)
        const sql =`
        insert into categories (${colunms.join(",")}) values('${values.join("','")}');
        `

        const r = await db(sql)

        await addReport(req.body.users_id,"create categories",data)

        res.send({
            type : "ok",
            result : r
        })
        
    } catch (error) {
        return res.status(500).send({
            type : "error",
            error ,
            msj :`${error}`  
        })
    }
    

}

module.exports = postCategories