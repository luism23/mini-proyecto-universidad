require('module-alias/register')

module.exports = {
    get: require("@controllers/categories/get"),
    delete: require("@controllers/categories/delete"),
    post: require("@controllers/categories/post"),
    put: require("@controllers/categories/put")
}