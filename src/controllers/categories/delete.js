require('module-alias/register')
const db = require("@app/db")
const addReport = require("@functions/addReport");


const deleteCategories = async (req,res)=>{
   try {
    const id = req.query.id

   
    const sql =`
    delete from categories
    where id= '${id}' ;
    `

    const r = await db(sql)

    await addReport(req.query.users_id,"delete categories",req.query)
    
    res.send({
        type : "ok",
        result : r
    }) 
   } catch (error) {
    return res.status(500).send({
        type : "error",
        error ,
        msj :`${error}`  
    })
   }
}

module.exports = deleteCategories