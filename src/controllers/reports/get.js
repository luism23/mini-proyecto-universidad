require("module-alias/register");
const db = require("@app/db");

const getReports = async (req, res) => {
    try {
        const sql = `
        select * from reports 
        `;
        const result = await db(sql);

        res.send({
            type: "ok",
            result: result.rows.map((e)=>{
                e.data = JSON.parse(e.data)
                return e
            })
        });
    } catch (error) {
        return res.status(500).send({
            type: "error",
            error,
            msj: `${error}`,
        });
    }
};

module.exports = getReports;
