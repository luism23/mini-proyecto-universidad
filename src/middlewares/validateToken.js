const jwt = require('jsonwebtoken')
const dotenv = require('dotenv').config()
const env = dotenv.parsed

const validateToken =(req,res,next)=>{
    try {
        const auth = req.headers.authorization

        if(!auth){
            throw new Error("error Token invalido")
        }
        
        const token = auth.split(" ").pop()
        const result = jwt.verify(token, env.JWT)
        
        if (!result.id) {
            throw new Error('Token invalid')
        }

        req.body.users_id = result.id+""

        req.query.users_id = result.id+""


    } catch (error) {
        return res.status(401).send({
            type : "error",
            error ,
            msj :`${error}`  ,
            token:"error"
        })
    }

    next()

}

module.exports = validateToken