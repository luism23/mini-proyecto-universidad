require('module-alias/register')
const routes = require("express").Router()
const reports = require("@controllers/reports/_index")
const fmiddlewares = require('fmiddlewares')

routes.get(
    "/",
    [

    ],
    reports.get
)

module.exports = routes