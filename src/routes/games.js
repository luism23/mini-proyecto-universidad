require('module-alias/register')
const routes = require("express").Router()
const games = require("@controllers/games/_index")
const fmiddlewares = require('fmiddlewares')
const validateToken = require("@middlewares/validateToken")

routes.get(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            users_id:{
                type:"string"
            }
        },"query")
    ],
    games.get
)
routes.delete(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            id:{
                type:"string"
            }
        },"query")
    ],
    games.delete
)
routes.post(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            name:{
                type:"string"
            },
            description:{
                type:"string"
            },
            categories_id:{
                type:"string"
            },
            users_id:{
                type:"string"
            }
        })
    ], 
    games.post
)
routes.put(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            data:{
                type:"object",
                items:{
                    exactItems:true,
                    name:{
                        
                         type:"string" ,
                         isUndefined:true                
                    },
                    description:{
                        type:"string",
                        isUndefined:true
                    },
                    categories_id:{
                        type:"string",
                        isUndefined:true
                    }
                    
                }
            },
            where:{
                type:"object",
                 items:{
                     id:{
                        type:"string"
                     }
                }
            }
        })
    ], 
    games.put
)

module.exports = routes