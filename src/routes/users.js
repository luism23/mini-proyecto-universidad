require('module-alias/register')
const routes = require("express").Router()
const users = require("@controllers/users/_index")
const fmiddlewares = require('fmiddlewares')

routes.post(
    "/register",
    [
        fmiddlewares.validateItem({
            email:{
                type:"email"
            },
            password:{
            type:"password",
            
            regexs:[
            {
                regex:/^.{8,}$/,
                msj:"minimum of 8 characters"
            },
            {
                regex:/[a-z]/,
                msj:"must contain lowercase letters"
            },
            {
                regex:/[A-Z]/,
                msj:"must contain capital letters"
            },
        ]
    }
        })
    ],
    users.register
)

routes.post(
    "/login",
    [
        fmiddlewares.validateItem({
            email:{
                type:"email"
            },
            password:{
            type:"password",
            
            regexs:[
            {
                regex:/^.{8,}$/,
                msj:"minimum of 8 characters"
            },
            {
                regex:/[a-z]/,
                msj:"must contain lowercase letters"
            },
            {
                regex:/[A-Z]/,
                msj:"must contain capital letters"
            },
        ]
    }
        })
    ],
    users.login
)

module.exports = routes