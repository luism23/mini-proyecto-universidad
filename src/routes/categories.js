require('module-alias/register')
const routes = require("express").Router()
const categories = require("@controllers/categories/_index")
const fmiddlewares = require('fmiddlewares')
const validateToken = require("@middlewares/validateToken")


routes.get(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            users_id:{
                type:"string"
            }
        },"query")
    ],
    categories.get
)
routes.delete(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            id:{
                type:"string"
            }
        },"query")
    ], 
    categories.delete
)
routes.post(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            name : {
                type:"string"
            },
            description:{
                type:"string"
            },
            users_id:{
                type:"string"
            },
        })
    ], 
    categories.post
)
    
routes.put(
    "/",
    [
        validateToken,
        fmiddlewares.validateItem({
            data:{
                type:"object",
                items:{
                    exactItems:true,
                    name:{
                        type:"string",
                        isUndefined:true
                    },
                    description:{
                        type:"string",
                        isUndefined:true
                    },
                    
                }

            },
            where:{
                tipe:"object",
                items:{
                    id:{
                        type:"string"
                    }
                }
            }
        })
    ], 
    categories.put
)

module.exports = routes