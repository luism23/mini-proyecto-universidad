require('module-alias/register')
const routes = require("express").Router()

const categories = require("@routes/categories")
const games = require("@routes/games")
const users = require("@routes/users")
const reports = require("@routes/reports")

routes.use("/categories",categories)
routes.use("/games",games)
routes.use("/users",users)
routes.use("/reports",reports)

module.exports = routes
