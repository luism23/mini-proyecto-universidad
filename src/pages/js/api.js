console.log("api cargada")
const request = async (settings) => {
    try {
        const headersList = {
            "Accept": "*/*",
            "User-Agent": "Thunder Client (https://www.thunderclient.io)",
            "Content-Type": "application/json",
            "Authorization":`Bearer ${localStorage.getItem('token')}`
        }
        const config = { 
            method: settings.method,
            headers: headersList
        }
        if(settings.method != "GET"){
            config.body = JSON.stringify(settings.body)
        }
        const respond = await fetch(`http://localhost:3001/api/v1/${settings.rute}`, config)
        const result = await respond.json()
        
        if(result.token === "error"){
            console.log(result);
            localStorage.removeItem('token')
            window.location.href = "./login.html"
            throw new Error("exit")
        }

        return result
    } catch (error) {
        console.log("error",error);
        alert(`Server Error`)
        throw new Error("Error Connect Server")
    }
}


const addCategories = async (data) => {
    console.log(data)
    const result = await request({
        rute:"categories",
        method:"POST",
        body:data
    })
    console.log(result);
    

    if(result.type == "ok"){
        btnCancelCategories.click()
        return result.result
    }else{
        alert(result.msj)
    }
}
const deleteCategories = async (id) => {
    const result = await request({
        rute:`categories?id=${id}`,
        method:"DELETE",
        body:{}
    })
    console.log(result);
    if(result.type == "ok"){
        return result.result
    }else{
        alert(result.msj)
    }
}
const updateCategories = async (data) => {
    const id = data.id
    delete data.id
    const result = await request({
        rute:`categories`,
        method:"PUT",
        body: {
            data,
            where:{
                id
            }
        }
    })
    console.log(result);
    

    if(result.type == "ok"){
        btnCancelCategories.click()
        return result.result
    }else{
        alert(result.msj)
    }
}
const getCategories = async () => {
    const result = await request({
        rute:"categories",
        method:"GET",
    })
    console.log(result);

    if(result.type == "ok"){
        listCategories = result.result
        return result.result
    }else {
        alert(result.msj)
    }
}
const addGame = async (data) => {
    const result = await request({
        rute:"games",
        method:"POST",
        body: data
    })
    console.log(result);

    if(result.type == "ok"){
        btnCancelGames.click()
        return result.result
    }else{
        alert(result.msj)
    }
}
const deleteGame = async (id) => {
    const result = await request({
        rute:`games?id=${id}`,
        method:"DELETE",
        body: {}
    })
    console.log(result);
    if(result.type == "ok"){
        return result.result
    }else{
        alert(result.msj)
    }
}
const updateGame = async (data) => {
    const id = data.id
    delete data.id
    const result = await request({
        rute:`games`,
        method:"PUT",
        body: {
            data,
            where:{
                id
            }
        }
    })
    console.log(result);

    if(result.type == "ok"){
        btnCancelGames.click()
        return result.result
    }else{
        alert(result.msj)
    }
}
const getGame = async () => {
        const result = await request({
        rute:"games",
        method:"GET",
    })
    console.log(result);
    if(result.type == "ok"){
        return result.result
    }else{
        alert(result.msj)
    }
}
const login = async (data) => {
    const result = await request({
        rute:"users/login",
        method:"POST",
        body:data
    })
    console.log(result);
    if(result.type == "ok"){
        return result
    }else{
        alert(result.msj)
        return result
    }
}
const register = async (data) => {
    const result = await request({
        rute:"users/register",
        method:"POST",
        body:data
    })
    console.log(result);
    if(result.type == "ok"){
        return result
    }else{
        alert(result.msj)
        return result
    }
}

const reports = async()=>{
    const result = await request({
        rute:"reports",
        method:"GET"
    })
    console.log(result);
    if(result.type == "ok"){
        return result
    }else{
        alert(result.msj)
        return result
    }
}

