const tableBodyReports = document.getElementById("tableBodyReports");


const loadReports = async () => {
    const result = await reports();
    if (result.type == "ok") {
        const list = result.result;
        const htmlList = list.map((e) => {
            const date = new Date(parseInt(e.create_time));
            return `
                <tr>
                    <td>${e.id}</td>
                    <td>${date.toISOString()}</td>
                    <td>${e.users_id}</td>
                    <td>${e.action}</td>
                    <td>${printData(e.data)}</td>
                </tr>
            `;
        });
        const html = htmlList.join("");
        tableBodyReports.innerHTML = html;
        window.print()
        window.close()
    }
};

const printData = (data) => {
    if(data == null || data == undefined){
        return "";
    }
    if(Array.isArray(data)){
        var r = "<ul>"
        for (var i = 0; i < data.length; i++) {
            const element = data[i];
            r = "<li>"+printData(element)+"</li>"
        }
        return r + "</ul>"
    }else if(typeof data === "object"){
        const keys = Object.keys(data)
        var r = "<ul>"
        for (var i = 0; i < keys.length; i++) {
            const key = keys[i];
            r = `
                <li>
                    <strong>${key}</strong>
                    ${printData(data[key])}
                </li>
            `
        }
        return r + "</ul>"
    }
    return data;
};

window.addEventListener("load", () => {
    loadReports();
});
