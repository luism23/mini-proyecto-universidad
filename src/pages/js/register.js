const inputEmail = document.getElementById("email")
const inputPassword = document.getElementById("password")
const btnSubmint = document.getElementById("btnSubmint")

btnSubmint.addEventListener('click',async (e)=>{
    e.preventDefault()
    const data = {
        email : inputEmail.value,
        password : inputPassword.value,
    }
    const result = await register(data);
    if(result.type == "ok"){
        window.location.href = "./login.html"
    }
})