console.log("aplicacion cargada")

var action = "add"
var idTarget = -1
var listCategories = []

const titleModalGames = document.getElementById("titleModalGames")
const titleModalCategories = document.getElementById("titleModalCategories")

const btnAddGames = document.getElementById("addGames")
const btnAddCategories = document.getElementById("addCategories")

btnAddGames.addEventListener('click',(e)=>{
    if(listCategories.length === 0){
        e.preventDefault()
        alert("Debes agregar una Categoria primero")
        setTimeout(()=>{
            btnCancelGames.click()
        },500)
        return;
    }
    titleModalGames.innerHTML = "Agregar"
    action = "add"
})
btnAddCategories.addEventListener('click',()=>{
    titleModalCategories.innerHTML = "Agregar"
    action = "add"
})


const tableGames = document.getElementById("tableGames")
const tableCategories = document.getElementById("tableCategories")

tableGames.addEventListener('click', async (element)=>{
    const target = element.target
    if(target.classList.value.split(" ").includes('editar')){
        titleModalGames.innerHTML = "Editar"
        action = "edit"
        idTarget = target.getAttribute("dataid");
        nameGames.value = target.getAttribute("dataname");
        descriptionGames.value = target.getAttribute("datad");
        selectCategories.value = target.getAttribute("datac");
    }else if(target.classList.value.split(" ").includes('eliminar')){
        action = "delete"
        idTarget = target.getAttribute("dataid");
        await deleteGame(idTarget)
        await loadDataGames()
    }
})
tableCategories.addEventListener('click',async(element)=>{
    const target = element.target
    if(target.classList.value.split(" ").includes('editar')){
        titleModalCategories.innerHTML = "Editar"
        action = "edit"
        idTarget = target.getAttribute("dataid");
        nameCategories.value = target.getAttribute("dataname");
        descriptionCategories.value = target.getAttribute("datad");
    }else if(target.classList.value.split(" ").includes('eliminar')){
        action = "delete"
        idTarget = target.getAttribute("dataid");
        await deleteCategories(idTarget)
        await loadDataCategories()
    }
})


const formModalGames = document.getElementById("formModalGames")
const nameGames = document.getElementById("nameGames")
const descriptionGames = document.getElementById("descriptionGames")
const selectCategories = document.getElementById("selectCategories")

formModalGames.addEventListener('submit',async(element)=>{
    element.preventDefault()
    const name = nameGames.value
    const description = descriptionGames.value
    const categories_id = selectCategories.value

    const data = {
        name,
        description,
        categories_id,
    }
    if(action == "add"){
        await addGame(data);
        await loadDataGames()
    }else if(action == "edit"){
        data.id = idTarget
        await updateGame(data);
        await loadDataGames()
    }
})
const formModalCategories = document.getElementById("formModalCategories")
const nameCategories = document.getElementById("nameCategories")
const descriptionCategories = document.getElementById("descriptionCategories")

formModalCategories.addEventListener('submit',async (element)=>{
    element.preventDefault()
    const name = nameCategories.value
    const description = descriptionCategories.value

    const data = {
        name,
        description,
    }
    if(action == "add"){
        await addCategories(data);
        await loadDataCategories()
    }else if(action == "edit"){
        data.id = idTarget
        await updateCategories(data);
        await loadDataCategories()
    }
})

const loadDataCategories = async () => { 
    const data = await getCategories()
    const options = data.map((element)=>{
        return `
        <option value="${element.id}">
            ${element.name}
        </option>
        `
    })
    selectCategories.innerHTML = options.join("")
    const htmls = data.map((element)=>{
        return `
        <tr>
            <th scope="row">${element.id}</th>
            <td>${element.name}</td>
            <td>${element.description}</td>
            <td><button dataId="${element.id}" dataname="${element.name}" datad="${element.description}" type="button" class="btn btn-info editar" data-bs-toggle="modal" data-bs-target="#modalCategories">Editar</button></td>
            <td><button dataId="${element.id}" type="button" class="btn btn-danger eliminar">Eliminar</button></td> 
        </tr>
        `
    })
    tableCategories.tBodies[0].innerHTML = htmls.join("")
}

const loadDataGames = async () => {
    const data = await getGame()
    const htmls = data.map((element)=>{
        return `
        <tr>
            <th scope="row">${element.id}</th>
            <td>${element.name}</td>
            <td>${element.description}</td>
            <td>${element.categories_id}</td>
            <td><button dataId="${element.id}" dataname="${element.name}" datad="${element.description}" datac="${element.categories_id}" type="button" class="btn btn-info editar" data-bs-toggle="modal" data-bs-target="#modalGames">Editar</button></td>
            <td><button dataId="${element.id}" type="button" class="btn btn-danger eliminar">Eliminar</button></td> 
        </tr>
        `
    })
    tableGames.tBodies[0].innerHTML = htmls.join("")
}
const loadAll = async () => {
    await loadDataGames()
    await loadDataCategories()
}
window.addEventListener('load',()=>{
    loadAll()
})

const logout = document.getElementById("logout")
logout.addEventListener('click',()=>{
    localStorage.removeItem('token')
    window.location.href = "./login.html"
})

const btnSendGames = document.getElementById("btnSendGames")
const btnSendCatergories = document.getElementById("btnSendCategories")
const btnCancelGames = document.getElementById("btnCancelGames")
const btnCancelCategories = document.getElementById("btnCancelCategories")
