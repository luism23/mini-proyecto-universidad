require('module-alias/register')
const db = require("@app/db")


const addReport = async (users_id,action,data)=>{
    data= JSON.stringify(data)
    const create_time = (new Date()).getTime()
    const json = {
        create_time,users_id,action,data
    }
    
    const colunms = Object.keys(json)
    const values = Object.values(json)

    const sql =`
    insert into reports (${colunms.join(",")}) values('${values.join("','")}'); 
    `

    const result = await db(sql)
    return result
}

module.exports = addReport