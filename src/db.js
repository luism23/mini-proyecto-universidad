const { Client } = require('pg')
const dotenv = require('dotenv').config()
const env = dotenv.parsed


const connect = async ()=>{
    const connectionData = {
        connectionString : env.DATABASE_URL
    }

    const client = new Client(connectionData)
    
    await client.connect()
    return client
}

const db = async (sql)=>{
    const client = await connect()
    const r = await client.query(sql)
    await client.end()
    return r
}

module.exports= db
