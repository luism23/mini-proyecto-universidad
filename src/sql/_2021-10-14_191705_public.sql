DROP TABLE IF EXISTS users;
CREATE TABLE users (  
    id SERIAL NOT NULL primary key,
    create_time NUMERIC,
    update_time NUMERIC,
    email varchar(255) UNIQUE,
    password varchar(255)
);
DROP TABLE IF EXISTS categories;
CREATE TABLE categories(
    id serial PRIMARY KEY,
    create_time numeric,
    update_time numeric,
    name character varying,
    description character varying,
    users_id INTEGER,
    CONSTRAINT fk_users
      FOREIGN KEY(users_id) 
	  REFERENCES users(id)
);

DROP TABLE IF EXISTS games;
CREATE TABLE games(
    id serial PRIMARY KEY,
    categories_id integer,
    create_time numeric,
    update_time numeric,
    name character varying,
    description character varying,
    CONSTRAINT fk_categories
      FOREIGN KEY(categories_id) 
	  REFERENCES categories(id),
    users_id INTEGER,
    CONSTRAINT fk_users
      FOREIGN KEY(users_id) 
	  REFERENCES users(id)
);